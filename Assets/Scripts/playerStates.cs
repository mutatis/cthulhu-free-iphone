﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum PlayerStates
{
    Run,
    ReceiveDamage,
    Jump,
    Fly,
    DoubleJump,
    FlapWings,
    Angry,
    Surprise,
}
